import { TemplateFormAppPage } from './app.po';

describe('template-form-app App', () => {
  let page: TemplateFormAppPage;

  beforeEach(() => {
    page = new TemplateFormAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
