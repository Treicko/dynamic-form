import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'prettyJson'
})
export class PrettyJsonPipe implements PipeTransform {
  transform(val) {
    return JSON.stringify(val, null, 4);
  }
}
