import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { PrettyJsonPipe } from './pretty-json.pipe';
import { KeysPipe } from './keys.pipe';
import { GeneratorFormComponent } from './generator-form/generator-form.component';

@NgModule({
  declarations: [
    AppComponent,
    PrettyJsonPipe,
    KeysPipe,
    GeneratorFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
