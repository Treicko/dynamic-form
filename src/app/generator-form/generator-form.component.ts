import { Component, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { Element } from './../models/element.model';
import { IElement } from './../models/element.model';

@Component({
  selector: 'app-generator-form',
  templateUrl: './generator-form.component.html',
  styleUrls: ['./generator-form.component.css']
})

export class GeneratorFormComponent implements OnChanges, OnInit {
  @Input() elements: Element[] = [];
  @Output() submit = new EventEmitter<any>();

  generatedModel = {};

  constructor(
    private elementRef: ElementRef
  ) { }

  getAttribute(attributte: string, index: number) {
    const att = this.elements[index][attributte] ? this.elements[index][attributte] : null;
    return this.isNameAttribute(att, index);
  }

  getPattern(attributte: string, index: number) {
    return this.elements[index][attributte] ? this.elements[index][attributte] : '.*';
  }

  getEvent(event: string, index: number, elem) {
    if (!this.elements[index]['events']) {
      return null;
    } else {
      this.isSubmitEvent(this.elements[index]['type']);
      return (!this.elements[index]['events'][event]) ? null : this.elements[index]['events'][event](elem);
    }
  }

  private isSubmitEvent(type: string) {
    if (type === 'submit') {
      for (let model in this.generatedModel) {
        this.elements.forEach(el => {
          if(el.name == model) {
            this.generatedModel[model] = (el.value) ? el.value : null;
          }
        });
      };
      this.submit.emit(this.generatedModel);
    }
  }

  private isNameAttribute(att, index) {
    if (att !== null) {
      this.generatedModel[att] = (this.elements[index]['value']) ? this.elements[index]['value'] : null;
    }
    return att;
  }
}
