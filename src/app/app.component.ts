import { Component, OnInit } from '@angular/core';
import { JsonModels } from './models/models';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  models: any[];
  elements: any;
  selectedModel: any;
  indexModel = 0;
  newModel: any;

  ngOnInit() {
    this.models = JsonModels;
    this.getModelAndComponetSelected();
  }

  getModelAndComponetSelected() {
    const newObject = this.models[this.indexModel].jsonObject;
    this.selectedModel = newObject;
  }

  showNewModel($event) {
    this.newModel = $event;
  }

}
