export const first = [
  {
    label: 'type button',
    type: 'button',
    value: 'Show message',
    events: {
      click: () => {
        alert('Welcome to Leos form');
      }
    }
  },
  {
    label: 'I have a bike',
    name: 'bike',
    type: 'checkbox',
    value: 'Bike',
    events: {
      blur: (element) => { console.log('Your changes has been saved'); }
    }
  },
  {
    label: 'favorite color',
    type: 'color',
    events: {
      click: () => {
        alert(`Select your favorite color`);
      }
    }
  },
  {
    label: 'birthday:',
    name: 'myBirthday',
    type: 'date',
  },
  {
    label: 'email:',
    name: 'myEmai',
    type: 'email',
  },
  {
    label: 'Select a file:',
    name: 'myFile',
    type: 'file'
  },
  {
    label: 'This is your reality:',
    name: 'imageUser',
    type: 'image',
    src: 'assets/finsemestre.jpg',
    width: 100,
    height: 150
  },
  {
    label: 'Quantity between (1 and 5):',
    name: 'salario',
    type: 'number',
    min: 1,
    max: 5
  },
  {
    label: 'Password:',
    name: 'password',
    type: 'password',
    maxlength: 8
  },
  {
    label: 'Male:',
    name: 'gender',
    type: 'radio'
  },
  {
    label: 'Female:',
    name: 'gender',
    type: 'radio'
  },
  {
    label: 'Do not know not answer',
    name: 'gender',
    type: 'radio'
  },
  {
    label: 'Points:',
    name: 'myPoints',
    type: 'range',
    min: 0,
    max: 10
  },
  {
    label: 'Search Google:',
    type: 'search'
  },
  {
    label: 'textitooooooooooo',
    name: 'description',
    type: 'text',
    value: 'My first text',
    events: {
      blur: () => { console.log('Your changes has been saved'); }
    }
  },
  {
    label: 'Dissabled text',
    type: 'text',
    value: 'My first text',
    disabled: true
  },
  {
    label: 'required text',
    type: 'text',
    required: 'required'
  },
  {
    label: 'placeholder text',
    type: 'text',
    placeholder: 'Type some text'
  },
  {
    label: 'read only text',
    type: 'text',
    value: 'this is read only text',
    readOnly: true
  },
  {
    label: 'pattern text',
    type: 'text',
    pattern: '^[0-9]*$',
  },
  {
    label: 'Select a time',
    type: 'time'
  },
  {
    label: 'Add your homepage',
    type: 'url'
  },
  {
    type: 'reset',
    value: 'Reset'
  },
  {
    type: 'submit',
    value: 'Submit',
    events: {
      click: () => {
        alert(`Successful register`);
      }
    }
  }
];
